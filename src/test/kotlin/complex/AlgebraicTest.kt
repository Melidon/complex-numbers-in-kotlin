package complex

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.math.atan2
import kotlin.math.sqrt
import kotlin.test.assertEquals

class AlgebraicTest {
    companion object {
        const val absoluteTolerance = 0.000001

        @JvmStatic
        fun RealAndImaginary() = listOf(
            Arguments.of(0.0, 0.0),
            Arguments.of(0.0, 1.0),
            Arguments.of(1.0, 0.0),
            Arguments.of(1.0, 1.0),
        )
    }

    @ParameterizedTest
    @MethodSource("RealAndImaginary")
    fun TrivialGetter(real: Double, imaginary: Double) {
        // Arrange
        val expectedReal = real
        val expectedImaginary = imaginary

        // Act
        val result = Algebraic(real = real, imaginary = imaginary)

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("RealAndImaginary")
    fun NonTrivialGetter(real: Double, imaginary: Double) {
        // Arrange
        val expectedDistance = sqrt(real * real + imaginary * imaginary)
        val expectedAngle = atan2(imaginary, real)

        // Act
        val result = Algebraic(real = real, imaginary = imaginary)

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("RealAndImaginary")
    fun toStringTest(real: Double, imaginary: Double) {
        // Arrange
        val algebraic = Algebraic(real = real, imaginary = imaginary)

        // Act
        val result = algebraic.toString()

        // Assert
        assertEquals("Algebraic(real=$real, imaginary=$imaginary)", result)
    }
}