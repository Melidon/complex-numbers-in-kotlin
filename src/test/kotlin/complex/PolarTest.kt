package complex

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.test.assertEquals

class PolarTest {
    companion object {
        const val absoluteTolerance = 0.000001

        @JvmStatic
        fun DistanceAndAngle() = listOf(
            Arguments.of(0.0, 0.0),
            Arguments.of(1.0, 0.0),
            Arguments.of(1.0, PI / 2),
            Arguments.of(sqrt(2.0), PI / 4),
        )
    }

    @ParameterizedTest
    @MethodSource("DistanceAndAngle")
    fun TrivialGetter(distance: Double, angle: Double) {
        // Arrange
        val expectedDistance = distance
        val expectedAngle = angle

        // Act
        val result = Polar(distance = distance, angle = angle)

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DistanceAndAngle")
    fun NonTrivialGetter(distance: Double, angle: Double) {
        // Arrange
        val expectedReal = distance * cos(angle)
        val expectedImaginary = distance * sin(angle)

        // Act
        val result = Polar(distance = distance, angle = angle)

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DistanceAndAngle")
    fun toStringTest(distance: Double, angle: Double) {
        // Arrange
        val polar = Polar(distance = distance, angle = angle)

        // Act
        val result = polar.toString()

        // Assert
        assertEquals("Polar(distance=$distance, angle=$angle)", result)
    }
}