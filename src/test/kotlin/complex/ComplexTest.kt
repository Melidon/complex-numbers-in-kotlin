package complex

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.math.*
import kotlin.test.assertEquals

class ComplexTest {

    companion object {
        const val absoluteTolerance = 0.000001

        @JvmStatic
        fun ComplexAndDouble() = listOf(
            Arguments.of(Algebraic(real = 1.0, imaginary = 1.0), 1.0),
            Arguments.of(Polar(distance = sqrt(2.0), angle = PI / 4), 1.0),
        )

        @JvmStatic
        fun DoubleAndComplex() = listOf(
            Arguments.of(1.0, Algebraic(real = 1.0, imaginary = 1.0)),
            Arguments.of(1.0, Polar(distance = sqrt(2.0), angle = PI / 4)),
        )

        @JvmStatic
        fun ComplexAndComplex() = listOf(
            Arguments.of(Algebraic(real = 1.0, imaginary = 1.0), Algebraic(real = 1.0, imaginary = 1.0)),
            Arguments.of(Algebraic(real = 1.0, imaginary = 1.0), Polar(distance = sqrt(2.0), angle = PI / 4)),
            Arguments.of(Polar(distance = sqrt(2.0), angle = PI / 4), Algebraic(real = 1.0, imaginary = 1.0)),
            Arguments.of(Polar(distance = sqrt(2.0), angle = PI / 4), Polar(distance = sqrt(2.0), angle = PI / 4)),
        )
    }

    @ParameterizedTest
    @MethodSource("ComplexAndDouble")
    fun ComplexPlusDouble(complex: Complex, double: Double) {
        // Arrange
        val expectedReal = complex.real + double
        val expectedImaginary = complex.imaginary

        // Act
        val result = complex + double

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DoubleAndComplex")
    fun DoublePlusComplex(double: Double, complex: Complex) {
        // Arrange
        val expectedReal = double + complex.real
        val expectedImaginary = complex.imaginary

        // Act
        val result = double + complex

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndComplex")
    fun ComplexPlusComplex(complex1: Complex, complex2: Complex) {
        // Arrange
        val expectedReal = complex1.real + complex2.real
        val expectedImaginary = complex1.imaginary + complex2.imaginary

        // Act
        val result = complex1 + complex2

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndDouble")
    fun ComplexMinusDouble(complex: Complex, double: Double) {
        // Arrange
        val expectedReal = complex.real - double
        val expectedImaginary = complex.imaginary

        // Act
        val result = complex - double

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DoubleAndComplex")
    fun DoubleMinusComplex(double: Double, complex: Complex) {
        // Arrange
        val expectedReal = double - complex.real
        val expectedImaginary = -complex.imaginary

        // Act
        val result = double - complex

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndComplex")
    fun ComplexMinusComplex(complex1: Complex, complex2: Complex) {
        // Arrange
        val expectedReal = complex1.real - complex2.real
        val expectedImaginary = complex1.imaginary - complex2.imaginary

        // Act
        val result = complex1 - complex2

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndDouble")
    fun ComplexTimesDouble(complex: Complex, double: Double) {
        // Arrange
        val expectedDistance = complex.distance * double
        val expectedAngle = complex.angle

        // Act
        val result = complex * double

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DoubleAndComplex")
    fun DoubleTimesComplex(double: Double, complex: Complex) {
        // Arrange
        val expectedDistance = double * complex.distance
        val expectedAngle = complex.angle

        // Act
        val result = double * complex

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndComplex")
    fun ComplexTimesComplex(complex1: Complex, complex2: Complex) {
        // Arrange
        val expectedDistance = complex1.distance * complex2.distance
        val expectedAngle = complex1.angle + complex2.angle

        // Act
        val result = complex1 * complex2

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndDouble")
    fun ComplexDivDouble(complex: Complex, double: Double) {
        // Arrange
        val expectedDistance = complex.distance / double
        val expectedAngle = complex.angle

        // Act
        val result = complex / double

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DoubleAndComplex")
    fun DoubleDivComplex(double: Double, complex: Complex) {
        // Arrange
        val expectedDistance = double / complex.distance
        val expectedAngle = -complex.angle

        // Act
        val result = double / complex

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndComplex")
    fun ComplexDivComplex(complex1: Complex, complex2: Complex) {
        // Arrange
        val expectedDistance = complex1.distance / complex2.distance
        val expectedAngle = complex1.angle - complex2.angle

        // Act
        val result = complex1 / complex2

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("ComplexAndDouble")
    fun ComplexPowDouble(complex: Complex, double: Double) {
        // Arrange
        val expectedDistance = complex.distance.pow(double)
        val expectedAngle = complex.angle * double

        // Act
        val result = complex.pow(double)

        // Assert
        assertEquals(expectedDistance, result.distance, absoluteTolerance)
        assertEquals(expectedAngle, result.angle, absoluteTolerance)
    }

    @ParameterizedTest
    @MethodSource("DoubleAndComplex")
    fun DoublePowComplex(double: Double, complex: Complex) {
        // Arrange
        val expectedReal = double.pow(complex.real) * cos(complex.angle * ln(double))
        val expectedImaginary = double.pow(complex.real) * sin(complex.angle * ln(double))

        // Act
        val result = double.pow(complex)

        // Assert
        assertEquals(expectedReal, result.real, absoluteTolerance)
        assertEquals(expectedImaginary, result.imaginary, absoluteTolerance)
    }
}