package complex

import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class Algebraic(
    override val real: Double = 0.0,
    override val imaginary: Double = 0.0,
) : Complex {
    constructor(that: Complex) : this(real = that.real, imaginary = that.imaginary)

    override val distance: Double
        get() = sqrt(real * real + imaginary * imaginary)
    override val angle: Double
        get() = atan2(imaginary, real)

    override fun toString(): String = this.toAlgebraicString()
}