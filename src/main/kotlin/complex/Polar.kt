package complex

import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class Polar(
    override val distance: Double = 0.0,
    override val angle: Double = 0.0,
) : Complex {
    constructor(that: Complex) : this(distance = that.distance, angle = that.angle)

    override val real: Double
        get() = distance * cos(angle)
    override val imaginary: Double
        get() = distance * sin(angle)

    override fun toString(): String = this.toPolarString()
}