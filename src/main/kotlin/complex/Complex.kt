package complex

import kotlin.math.*

interface Complex {
    val real: Double
    val imaginary: Double
    val distance: Double
    val angle: Double

    override fun toString(): String
}

inline fun Complex.toAlgebraicString(): String = "Algebraic(real=$real, imaginary=$imaginary)"
inline fun Complex.toPolarString(): String = "Polar(distance=$distance, angle=$angle)"

inline operator fun Complex.plus(that: Double): Complex = this + Algebraic(real = that)
inline operator fun Double.plus(that: Complex): Complex = Algebraic(real = this) + that
inline operator fun Complex.plus(that: Complex): Complex = Algebraic(real = this.real + that.real, imaginary = this.imaginary + that.imaginary)

inline operator fun Complex.minus(that: Double): Complex = this - Algebraic(real = that)
inline operator fun Double.minus(that: Complex): Complex = Algebraic(real = this) - that
inline operator fun Complex.minus(that: Complex): Complex = Algebraic(real = this.real - that.real, imaginary = this.imaginary - that.imaginary)

inline operator fun Complex.times(that: Double): Complex = this * Algebraic(real = that)
inline operator fun Double.times(that: Complex): Complex = Algebraic(real = this) * that
inline operator fun Complex.times(that: Complex): Complex = Polar(distance = this.distance * that.distance, angle = this.angle + that.angle)

inline operator fun Complex.div(that: Double): Complex = this / Algebraic(real = that)
inline operator fun Double.div(that: Complex): Complex = Algebraic(real = this) / that
inline operator fun Complex.div(that: Complex): Complex = Polar(distance = this.distance / that.distance, angle = this.angle - that.angle)

/**
 * Raises a complex number to the power of a real number.
 *
 * Returns one of the possibly infinite solutions.
 */
inline fun Complex.pow(that: Double): Complex = Polar(distance = this.distance.pow(that), angle = this.angle * that)

/**
 * Raises a real number to the power of a complex number.
 *
 * Returns the only solution
 */
inline fun Double.pow(complex: Complex): Complex =
    this.pow(complex.real) * Algebraic(real = cos(complex.angle * ln(this)), imaginary = sin(complex.angle * ln(this)))

/**
 * Raises a complex number to the power of a real number.
 *
 * Returns one of the infinite solutions.
 */
inline fun Complex.pow(that: Complex): Complex = E.pow(Algebraic(real = ln(this.distance), imaginary = this.angle) * that)
